﻿using System;

namespace persistenceService
{
    class MailLog
    {
        public string Email { get; set; }
        public string Subject { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
