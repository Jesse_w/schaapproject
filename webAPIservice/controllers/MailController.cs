﻿using System.Web.Http;
using webAPIservice.managers;
using mail.model;

namespace webAPIservice.controllers
{
    public class MailController : ApiController
    {
        private MailManager mailManager;

        public MailController()
        {
            mailManager = new MailManager();
        }

        [HttpGet]
        [Route("")]
        public string ShowRun()
        {
            return "Email service is running";
        }

        [HttpPost]
        [Route("")]
        public void Log(Mail mail)
        {
            mailManager.SendMail(mail);
        }
    }
}
