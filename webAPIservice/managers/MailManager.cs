﻿using System;
using mail.model;
using Microsoft.ServiceFabric.Actors;
using Microsoft.ServiceFabric.Actors.Client;
using persistenceService.Interfaces;

namespace webAPIservice.managers
{
    public class MailManager
    {
        private System.Net.Mail.SmtpClient smtp;
        private string sendingMail = "microticonf@outlook.be";

        public MailManager()
        {
            smtp = new System.Net.Mail.SmtpClient();
            smtp.Port = 587;
            smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Host = "smtp.live.com";
            smtp.EnableSsl = true;
            smtp.Credentials = new System.Net.NetworkCredential(sendingMail, "ticonf2018");
        }

        public void SendMail(Mail mail)
        {
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            message.To.Add(mail.Email);
            message.Subject = mail.Subject;
            message.From = new System.Net.Mail.MailAddress(sendingMail);
            message.Body = mail.Body;

            smtp.Send(message);
            LogMail(mail);
        }

        private void LogMail(Mail mail)
        {
            // Create a randomly distributed actor ID
            ActorId actorId = ActorId.CreateRandom();

            // This only creates a proxy object, it does not activate an actor or invoke any methods yet.
            IpersistenceService myActor = ActorProxy.Create<IpersistenceService>(actorId, new Uri("fabric:/schaapProject/IpersistenceServiceActorService"));

            // This will invoke a method on the actor. If an actor with the given ID does not exist, it will be activated by this method call.
             myActor.addEmailLog(mail);
        }
    }
}
