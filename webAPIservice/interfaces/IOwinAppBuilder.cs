﻿using Owin;

namespace webAPIservice.interfaces
{
    interface IOwinAppBuilder
    {
        void Configuration(IAppBuilder appBuilder);


    }
}
